FROM quay.io/ansible/ansible-runner:latest

RUN ansible-galaxy collection install \
  git+https://gitlab.ethz.ch/ansible-community/collections/system_configuration.git \
  git+https://gitlab.ethz.ch/ansible-community/collections/system_management.git \
  git+https://gitlab.ethz.ch/ansible-community/collections/security.git
